plugins {
    kotlin("jvm") version "1.6.0"
}

group = "cn.liming.lucenelearn"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {

    implementation("org.apache.lucene:lucene-core:8.0.0")
    implementation("org.apache.lucene:lucene-analyzers-common:8.0.0")
    implementation("org.apache.lucene:lucene-queryparser:8.0.0")
    implementation("org.apache.lucene:lucene-highlighter:8.0.0")
    implementation("com.jianggujin:IKAnalyzer-lucene:8.0.0")
    implementation("org.apache.lucene:lucene-queries:8.0.0")
    implementation("org.apache.lucene:lucene-grouping:8.0.0")
    implementation("org.apache.lucene:lucene-suggest:8.0.0")

    //结巴分词器
    implementation("com.huaban:jieba-analysis:1.0.2")
    //IK分词器
    implementation("com.jianggujin:IKAnalyzer-lucene:8.0.0")

    implementation(kotlin("stdlib"))
}

<!--
    该readme文件，模仿Lucene(https://github.com/apache/lucene/blob/main/README.md?plain=1)
    的readme文件而写。
 -->

# Apache Lucene 学习笔记

![Lucene Logo](https://lucene.apache.org/theme/images/lucene/lucene_logo_green_300.png?v=0e493d7a)

Apache Lucene是一个用Java编写的高性能，功能齐全的文本搜索引擎库。这里我用Kotlin学习如何使用它

[![Build Status](https://ci-builds.apache.org/job/Lucene/job/Lucene-Artifacts-main/badge/icon?subject=Lucene)](https://ci-builds.apache.org/job/Lucene/job/Lucene-Artifacts-main/)

## Lucene在线文档

此自述文件仅包含当前练习项目的简单说明。 更多Lucene的全面的文档，请访问:

- Latest Releases: <https://lucene.apache.org/core/documentation.html>
- Nightly: <https://ci-builds.apache.org/job/Lucene/job/Lucene-Artifacts-main/javadoc/>
- Build System Documentation: [help/](./help/)
- Developer Documentation: [dev-docs/](./dev-docs/)
- Migration Guide: [lucene/MIGRATE.md](./lucene/MIGRATE.md)

## 使用 Gradle 进行构建

### 基本步骤：

0. 安装java1.8 (或更高级版本)；
1. 克隆本项目仓库 (或下载源代码分发版)；
2. 运行 gradle 启动器脚本 （'gradlew'）；

### Step 0) 设置开发环境（JDK1.8或更高版本）

我们假设您知道如何获取和设置 JDK - 如果您
不要，那么我们建议从 https://jdk.java.net/ 开始学习
在返回本自述文件之前，将进一步了解 Java。Lucene与一起奔跑
Java8 或更高版本。

Lucene 使用 [Gradle](https://gradle.org/) 生成控制. Gradle本身是基于Java的
并且可能与较新的Java版本不兼容;您仍然可以构建和测试
Lucene 与这些 Java 发行版, 查看 [jvms.txt](./help/jvms.txt) 获取更多信息.

注意: Lucene在9.0版中从Apache Ant更改为Gradle。先前版本
仍然使用Apache Ant。

### Step 1) 下载Lucene源代码

你可以在GitHub上下拉Lucene的源代码:

https://github.com/apache/lucene

或获取特定版本的 Lucene 源存档，从:

https://lucene.apache.org/core/downloads.html

下载源存档并将其解压缩到您选择的目录中。

### Step 2) 运行Gradle

运行 "./gradlew help", 这将显示可以执行的主要任务显示帮助子主题。

注意: 请勿使用可能安装在计算机上的`gradle`命令。这可能
导致使用与项目要求不同的等级版本，这是已知的
导致非常隐晦的错误。`gradle wrapper`（gradlew脚本）可以做任何事情
从头开始构建项目所需的：它下载了正确版本的gradle，
设置合理的本地配置，并在多个环境中进行测试.

第一次运行 gradlew 时，它将创建一个文件"gradle.properties"，该文件
包含特定于计算机的设置。通常，您可以按原样使用此文件，但它
如有必要，可以进行修改。

`./gradlew check` 将组装Lucene并运行所有验证任务（包括测试）.

`./gradlew help`将打印一个介绍和解释的帮助指南列表构建系统的各个部分，包括典型的工作流任务。

如果只想生成文档，请执行:

```
./gradlew documentation
```

### IDE 支持

- *IntelliJ* - IntelliJ idea 可以开箱即用地导入和构建基于等级的项目；
- *Eclipse*  - Basic support ([help/IDEs.txt](https://github.com/apache/lucene/blob/main/help/IDEs.txt#L7)).
- *Netbeans* - Not tested.

## 贡献

始终欢迎错误修复，改进和新功能！
请查看 [Contributing to Lucene
Guide](https://cwiki.apache.org/confluence/display/lucene/HowToContribute) 了解有关贡献的信息。

## 讨论与支持

- [Users Mailing List](https://lucene.apache.org/core/discussion.html#java-user-list-java-userluceneapacheorg)
- [Developers Mailing List](https://lucene.apache.org/core/discussion.html#developer-lists)
- [Issue Tracker](https://issues.apache.org/jira/browse/LUCENE)
- IRC: `#lucene` and `#lucene-dev` on freenode.net